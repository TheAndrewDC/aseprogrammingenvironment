﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// The main Form class.
    /// Contains all methods for performing commands.
    /// </summary>
    public partial class Form1 : Form
    {

        Bitmap outputBitmap;
        Canvas myCanvas;
        //CommandParser parser;
        public ProgramStorage ps;

        /// <summary>
        /// Constructor for creating the program
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            outputBitmap = new Bitmap(OutputDisplay.Width, OutputDisplay.Height);
            myCanvas = new Canvas(Graphics.FromImage(outputBitmap));
            ps = new ProgramStorage(myCanvas);

        }
        //<summary>
        // Checks when return has been pressed in the small text box then sends the commands to be checked that they are valid
        // </summary>
        private void ShortCommandBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string command = ShortCommandBox.Text.ToLower();
                bool isValid = false;
                bool doNotRun = false;
                String errors = "Errors Found";
                if (command.Equals("run"))
                {
                    errorLabel.Text = "";
                    string[] longCommandLines = LongCommandBox.Text.Split('\n');

                    for (int i = 0; i < longCommandLines.Length; i++)
                    {
                        try
                        {
                            isValid = ps.parser.ValidCommand(longCommandLines[i], i);
                        }
                        catch (InvalidCommandException ex)
                        {
                            doNotRun = true;
                            errors = errors + ex.Message;
                        }
                        catch (InvalidParameterException ex)
                        {
                            doNotRun = true;
                            errors = errors + ex.Message;
                        }

                    }
                    if (doNotRun == false)
                    {
                        errorLabel.Text = "Commands were Valid. Excecuting now.";
                        ps.RunCommand();
                        ps.ifNumber = 1;
                    }
                    else
                    {
                        errorLabel.Text = errors;
                    }

                }
                else
                {
                    try
                    {
                        int lineNumber = 0;
                        isValid = ps.parser.ValidCommand(command, lineNumber);
                        if (isValid == true)
                        {
                            ps.RunCommand();
                        }

                    }
                    catch (InvalidCommandException a)
                    {
                        
                        errors = a.Message;
                        errorLabel.Text = errors;
                    }
                    catch(InvalidParameterException a)
                    {
                        errors = a.Message;
                        errorLabel.Text = a.Message;
                    }
                }
                //errorLabel.Text = parser.ErrorReturn();
                OutputDisplay.Image = outputBitmap;
                ShortCommandBox.Text = "";
            }
        }
        private void OutputDisplay_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(outputBitmap, 0, 0);
        }
        /// <summary>
        /// Makes the rich text field content be able to saved to a document
        /// </summary>
        private void SaveButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile1 = new SaveFileDialog
            {
                DefaultExt = "*.rtf",
                Filter = "RTF Files|*.rtf"
            };

            if (saveFile1.ShowDialog() == System.Windows.Forms.DialogResult.OK && saveFile1.FileName.Length > 0)
            {
                LongCommandBox.SaveFile(saveFile1.FileName);
            }
            else
            {
                Console.WriteLine("Unknwon");
            }
        }
        /// <summary>
        /// Makes the rich text field content be able to be loaded from a document
        /// </summary>
        private void LoadButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile1 = new OpenFileDialog
            {
                DefaultExt = "*.rtf",
                Filter = "RTF Files|*.rtf"
            };

            if (openFile1.ShowDialog() == System.Windows.Forms.DialogResult.OK && openFile1.FileName.Length > 0)
            {
                LongCommandBox.LoadFile(openFile1.FileName);
            }
        }
    }
}
