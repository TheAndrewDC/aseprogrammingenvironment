﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// Methods involving program storage
    /// </summary>
    public class ProgramStorage
    {
        private ArrayList var = new ArrayList();
        private ArrayList variableNames = new ArrayList();


        private List<Command> commandList = new List<Command>();

        public List<String> whileCommands = new List<String>();

        public List<String> ifCommands = new List<String>();
        public List<int> ifNumbers = new List<int>();
        public int ifCommandEnd;
        public int ifNumber = 1;
        public int ifStart;
        public int ifEnd;

        public List<String> loopCommands = new List<String>();
        public int numberOfLoops;
        public int loopLength = 0;

        public List<String> methodCommands = new List<String>();
        public int methodStart;
        public int methodEnd;



        DataTable dt = new DataTable();
        Canvas myCanvas;
        public CommandParser parser;
        public int command;
        public int ifCommandInt;
        public int loopStart;
        public int loopEnd;


        /// <summary>
        /// Program Storage constructor
        /// </summary>
        public ProgramStorage()
        {

        }

        /// <summary>
        /// Get the canvas value and store it in the class
        /// </summary>
        /// <param name="canvas">Sets the myCanvas value</param>
        public ProgramStorage(Canvas canvas)
        {
            this.myCanvas = canvas;
            parser = new CommandParser(myCanvas, this);
        }

        /// <summary>
        /// Used for adding variables
        /// </summary>
        /// <param name="V">V is the variable to be added</param>
        public void AddVariable(Variables V)
        {
            var.Add(V);
            variableNames.Add(V._name);
        }

        /// <summary>
        /// Searches for variable names
        /// </summary>
        /// <param name="_name">The name of the parameter to look for</param>
        /// <returns></returns>
        public int SearchVariableName(String _name)
        {
            _name = _name.Trim();
            for (int i = 0; i < var.Count; i++)
            {
                Variables v = (Variables)var[i];
                if (v._name.Equals(_name))
                    return i;
            }
            return -1;
        }

        /// <summary>
        /// Get the variable value;
        /// </summary>
        /// <param name="VarName">Variable name to search for </param>
        /// <returns>Value of the variable</returns>
        public int GetVariableValue(String VarName)
        {
            int index = this.SearchVariableName(VarName);
            if (index >= 0)
            {
                Variables v = (Variables)var[index];
                return v.VarValue;
            }
            else
            {
                throw new ApplicationException("Variable doesn't exist");
            }
        }

        /// <summary>
        /// Check if variable exist
        /// </summary>
        /// <param name="name">The name of the variable to look for</param>
        /// <returns>returns true if found and false if not</returns>
        public bool DoesVariableExist(string name)
        {
            if (variableNames.Contains(name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Set variable values
        /// </summary>
        /// <param name="VarName"> The variables name to be changed</param>
        /// <param name="Value">What to change the variable value to</param>
        public void SetVariableValue(String VarName, int Value)
        {
            int index = this.SearchVariableName(VarName);
            if (index >= 0)
            {
                Variables v = (Variables)var[index];
                v.VarValue = Value;
            }
            else
            {
                throw new ApplicationException("Variable doesn't exist");
            }
        }

        /// <summary>
        /// Add command to a list and increase command by 1;
        /// </summary>
        /// <param name="a">The command to be added to the list</param>
        public void AddCommand(Command a)
        {
            commandList.Add(a);
            command++;
        }


        /// <summary>
        /// Runs each command from the command list
        /// </summary>
        public void RunCommand()
        {
            for (int i = 0; i < command; i++)
            {
                Command C = commandList[i];

                if (C is IfCommand)
                {
                    int accessNumber = (ifNumber * 2) - 1;
                    ifStart = Int32.Parse(ifNumbers[accessNumber - 1].ToString());
                    ifEnd = Int32.Parse(ifNumbers[accessNumber].ToString());

                    String[] ifCommandLine = ifCommands.ToArray();
                    String[] theIfStatment = ifCommandLine[ifNumber - 1].Split(' ');
                    int ifLength = ifEnd - ifStart - 1;
                    int number1;

                    String expression;
                    number1 = this.GetVariableValue(theIfStatment[1]);
                    theIfStatment[1] = number1.ToString();
                    expression = String.Join("", theIfStatment.Skip(1));
                    var result = dt.Compute(expression, "").ToString();

                    if (result.Equals("True"))
                    {
                        i++;
                        for (int a = 0; a < ifLength; a++)
                        {
                            C = commandList[i + a];
                            C.Execute();
                        }
                        i = i + ifLength - 1;

                    }
                    else
                    {
                        i = i + ifLength;
                        C.Execute();
                    }
                    ifNumber++;
                }
                else if (C is WhileCommand)
                {
                    bool flag = false;
                    bool isValid = false;
                    while (flag == false)
                    {
                        String[] whileCommandString = whileCommands.ToArray();
                        String[] splitCommand = whileCommandString[0].Split(' ');
                        splitCommand[1] = this.GetVariableValue(splitCommand[1]).ToString();
                        String expression;
                        expression = String.Join("", splitCommand.Skip(1));
                        Console.WriteLine(expression);

                        var result = dt.Compute(expression, "").ToString();

                        if (!result.Equals("True"))
                        {
                            flag = true;
                        }
                        else
                        {
                            foreach (String dasdaw in whileCommandString.Skip(1))
                            {
                                isValid = parser.ValidCommand(dasdaw, 0);
                            }
                        }
                    }
                }
                else
                {
                    C.Execute();
                }



            }
        }


    }
}
