﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// Factory for creating the commands
    /// </summary>
    public class CommandFactory
    {
        /// <summary>
        /// Returns the desired class
        /// </summary>
        /// <param name="command">String with the command to be returned</param>
        /// <returns>Returns the desired class</returns>
        public Command GetCommand(string command)
        {
            command = command.ToUpper();

            if (command.Equals("CIRCLE"))
            {
                return new CircleCommand();
            }
            else if (command.Equals("CLEAR"))
            {
                return new ClearCommand();
            }
            else if (command.Equals("MOVETO"))
            {
                return new MoveToCommand();
            }
            else if (command.Equals("DRAWTO"))
            {
                return new DrawToCommand();
            }
            else if (command.Equals("PEN"))
            {
                return new PenCommand();
            }
            else if (command.Equals("RECTANGLE"))
            {
                return new RectangleCommand();
            }
            else if (command.Equals("RESET"))
            {
                return new ResetCommand();
            }
            else if (command.Equals("TRIANGLE"))
            {
                return new TriangleCommand();
            }
            else if (command.Equals("FILL"))
            {
                return new FillCommand();
            }
            else if (command.Equals("IF"))
            {
                return new IfCommand();
            }
            else if (command.Equals("WHILE"))
            {
                return new WhileCommand();
            }
            else
            {
                return null;
            }
        }

    }
}
