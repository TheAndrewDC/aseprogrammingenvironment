﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// Variables class with methods for creating variables
    /// </summary>
    public class Variables : Command
    {
        public String _name;
        public int _value;
        /// <summary>
        /// Variables constructor
        /// </summary>
        public Variables()
        {

        }

        /// <summary>
        /// Create variable with Name and Value
        /// </summary>
        /// <param name="name">Variable name</param>
        /// <param name="varValue">variable value</param>
        public Variables(string name, int varValue)
        {
            this._name = name;
            this._value = varValue;
        }

        /// <summary>
        /// Set and get the Name
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
                   
        }
        /// <summary>
        /// Set and get the variable value
        /// </summary>
        public int VarValue
        {
            get { return _value; }
            set { _value = value; }
        }

        /// <summary>
        /// Calculate the variable calculation and set it to the variable
        /// </summary>
        /// <param name="expression">Expression for the calculation</param>
        public void VariableCalculation(String expression)
        {
            DataTable dt = new DataTable();
            String result;
            result = dt.Compute(expression, "").ToString();
            _value = Int32.Parse(result);

            Console.WriteLine("Result equals: " + result);


        }
        /// <summary>
        /// Execute
        /// </summary>
        /// <returns>returns null</returns>
        public override Canvas Execute()
        {
            return null;
        }

    }
}
