﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// Base Command class
    /// </summary>
    public abstract class Command
    {
        public Canvas myCanvas;
        public int[] ParamsInt;
        public bool valid;

        /// <summary>
        /// Command constructor
        /// </summary>
        public Command()
        {

        }

        /// <summary>
        /// Constructor that gets the canvas content then sets it in the class
        /// </summary>
        /// <param name="myCanvas">Sets the canvas</param>
        public Command(Canvas myCanvas)
        {
            this.myCanvas = myCanvas;
        }


        /// <summary>
        /// Set the canvas and the parameter list
        /// </summary>
        /// <param name="myCanvas">Sets the canvas</param>
        /// <param name="list">Sets the parameter</param>
        public virtual void Set(Canvas myCanvas, params int[] list)
        {
            this.myCanvas = myCanvas;
            this.ParamsInt = list;
        }
        /// <summary>
        /// Used for executing the command
        /// </summary>
        /// <returns></returns>
        public abstract Canvas Execute();

    }
}
