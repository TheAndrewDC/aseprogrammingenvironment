﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// IfCommand used for ifs
    /// </summary>
    class IfCommand :Command
    {
        public List<String> ifCommands = new List<String>();
        public String[] stringHere;

        /// <summary>
        /// IfCommand constructor
        /// </summary>
        public IfCommand()
        {

        }
        /// <summary>
        /// Get the IfCommands
        /// </summary>
        /// <returns>returns ifcommands</returns>
        public String[] getIfCommands()
        {
            stringHere = ifCommands.ToArray();
            return stringHere;
        }

        /// <summary>
        /// Used for adding an if command from a string
        /// </summary>
        /// <param name="newCommand">the command to be added</param>
        public void AddIfCommand(string newCommand)
        {
            ifCommands.Add(newCommand);
        }
        /// <summary>
        /// Execute the command
        /// </summary>
        /// <returns>returns the canvas</returns>
        public override Canvas Execute()
        {
            return myCanvas;
        }


    }
}
