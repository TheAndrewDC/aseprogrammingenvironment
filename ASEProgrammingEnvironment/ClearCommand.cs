﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// ClearCommand stores methods for interacting with canvas
    /// </summary>
    class ClearCommand : Command
    {
        private Canvas _myCanvas;
        /// <summary>
        /// ClearCommand Constructor
        /// </summary>
        public ClearCommand()
        {
        }

        /// <summary>
        /// Clear Command constructor that sets the canvas
        /// </summary>
        /// <param name="myCanvas">Transfers Canvas to the class</param>
        public ClearCommand(Canvas myCanvas) : base(myCanvas)
        {
            _myCanvas = myCanvas;
        }
        /// <summary>
        /// Set the canvas
        /// </summary>
        /// <param name="myCanvas">Sets the canvas</param>
        public void Set(Canvas myCanvas)
        {
            this._myCanvas = myCanvas;
        }


        /// <summary>
        /// Execute the drawing of the command
        /// </summary>
        /// <returns>Returns the canvas</returns>
        public override Canvas Execute()
        {
            _myCanvas.ClearImage();
            return _myCanvas;
        }
    }
}
