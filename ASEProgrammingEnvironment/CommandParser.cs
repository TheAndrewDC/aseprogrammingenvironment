﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// Contains the methods for checking the commands are valid
    /// </summary>
    public class CommandParser
    {
        Canvas myCanvas;
        CommandFactory cf = new CommandFactory();
        List<Variables> variableList = new List<Variables>();
        ProgramStorage ps = new ProgramStorage();
        List<String> commandList = new List<String>();
        List<String> methodList = new List<String>();
        DataTable dt = new DataTable();
        IfCommand ifCommand;

        bool valid = true;
        bool loopFlag = false;
        bool methodFlag = false;
        bool whileFlag = false;





        private String[] acceptableCommands = { "clear", "reset", "pen", "fill", "circle", "triangle", "moveto", "drawto", "rectangle", "while", "endloop" };
        private String[] noNumberCommands = { "clear", "reset", "pen", "fill" };
        private String[] oneNumberCommands = { "circle", "triangle" };
        private String[] twoNumberCommands = { "rectangle", "moveto", "drawto" };
        String[] variableNames = { };
        private int number1;
        private int number2;


        /// <summary>
        /// Empty constructor for initialising the class
        /// </summary>
        public CommandParser()
        { }


        /// <summary>
        /// Constructor used for initialising the class
        /// </summary>
        /// <param name="myCanvas">Set Canvas object so can be accessed within the class</param>
        /// <param name="ps">Set the ProgramStorage</param>
        public CommandParser(Canvas myCanvas, ProgramStorage ps)
        {
            ifCommand = new IfCommand();
            this.myCanvas = myCanvas;
            this.ps = ps;
        }

        /// <summary>
        /// Number 1 storage
        /// </summary>
        public int Number1
        {
            get { return number1; }
            set { number1 = value; }
        }

        /// <summary>
        /// Number 2 storage
        /// </summary>
        public int Number2
        {
            get { return number2; }
            set { number2 = value; }
        }



        /// <summary>
        /// Checks if the command is valid and stores it to be ran later
        /// </summary>
        /// <param name="commandInput">String of the command to be checked</param>
        /// <returns>Returns true if the command is valid</returns>
        public bool ValidCommand(String commandInput, int commandNumber)
        {
            Console.WriteLine(commandNumber);
            commandInput = commandInput.ToLower().TrimStart();

            String[] wholeCommand = commandInput.Split(' ');
            String command = wholeCommand[0];


            if (oneNumberCommands.Any(command.Equals))
            {
                String parameter = wholeCommand[1];
                if (ps.DoesVariableExist(parameter) == true)
                {
                    number1 = ps.GetVariableValue(parameter);
                }
                else
                {
                    try
                    {
                        number1 = Int32.Parse(parameter);
                    }
                    catch
                    {
                        throw new InvalidParameterException(parameter, commandNumber + 1);
                    }
                }
            }
            else if (twoNumberCommands.Any(command.Equals))
            {
                String[] extraSplit = wholeCommand[1].Split(',');
                if (ps.DoesVariableExist(extraSplit[0]) == true)
                {
                    number1 = ps.GetVariableValue(extraSplit[0]);
                    if (ps.DoesVariableExist(extraSplit[1]) == true)
                    {
                        number2 = ps.GetVariableValue(extraSplit[1]);
                    }
                    else { try { number2 = Int32.Parse(extraSplit[1]); }catch { throw new InvalidParameterException(extraSplit[1], commandNumber + 1); } }
                }
                else if (ps.DoesVariableExist(extraSplit[1]) == true)
                {
                    number2 = ps.GetVariableValue(extraSplit[1]);
                    try { number1 = Int32.Parse(extraSplit[0]); } catch { throw new InvalidParameterException(extraSplit[1], commandNumber + 1); }
                }
                else
                {
                    try
                    {
                        number1 = Int32.Parse(extraSplit[0]);
                        number2 = Int32.Parse(extraSplit[1]);
                    }
                    catch
                    {
                        throw new InvalidParameterException(wholeCommand[1], commandNumber + 1);
                    }
                }

            }
            

            if (methodList.Contains(command))
            {
                this.CallMethod();
                return valid;
            }
            else if (methodFlag == true)
            {
                if (command.Equals("endmethod"))
                {
                    methodFlag = false;
                    return valid;
                }
                else
                {
                    ps.methodCommands.Add(commandInput);
                    return valid;
                }
            }
            
            else if (whileFlag == true)
            {
                if(command.Equals("endwhile"))
                {
                    whileFlag = false;
                    return valid;
                }
                else
                {
                    ps.whileCommands.Add(commandInput);
                    return valid;
                }
            }

            else if (loopFlag == true)
            {
                if (command.Equals("endloop"))
                {
                    loopFlag = false;
                    this.theLoop();
                    return valid;
                }
                else
                {
                    ps.loopCommands.Add(commandInput);
                    ps.loopLength = ps.loopLength + 1;
                    return valid;
                }
            }
            else
            {
                if (wholeCommand.Contains("="))
                {
                    if (wholeCommand.Length == 3)
                    {
                        Variables variable = new Variables(wholeCommand[0], Int32.Parse(wholeCommand[2]));
                        ps.AddVariable(variable);
                        return valid;
                    }
                    if (wholeCommand.Length > 3)
                    {
                        if (ps.DoesVariableExist(wholeCommand[2]) == true)
                        {
                            String expression;
                            number1 = ps.GetVariableValue(wholeCommand[2]);
                            wholeCommand[2] = number1.ToString();
                            expression = string.Join("", wholeCommand.Skip(2));

                            var resultA = dt.Compute(expression, "").ToString();
                            number1 = Int32.Parse(resultA);
                            ps.SetVariableValue(wholeCommand[0], number1);
                            return valid;
                        }
                    }
                }
                switch (command)
                {
                    case "clear":
                        ClearCommand a = (ClearCommand)cf.GetCommand("clear");
                        a.Set(myCanvas);
                        ps.AddCommand(a);
                        return valid;
                    case "reset":
                        ResetCommand b = (ResetCommand)cf.GetCommand("reset");
                        b.Set(myCanvas);
                        ps.AddCommand(b);
                        return valid;
                    case "circle":
                        CircleCommand c = (CircleCommand)cf.GetCommand("circle");
                        c.Set(myCanvas, number1);
                        ps.AddCommand(c);
                        return valid;
                    case "drawto":
                        DrawToCommand d = (DrawToCommand)cf.GetCommand("drawto");
                        d.Set(myCanvas, number1, number2);
                        ps.AddCommand(d);
                        return valid;
                    case "fill":
                        FillCommand e = (FillCommand)cf.GetCommand("fill");
                        e.Set(myCanvas, command[1]);
                        ps.AddCommand(e);
                        return valid;
                    case "moveto":
                        MoveToCommand f = (MoveToCommand)cf.GetCommand("moveto");
                        f.Set(myCanvas, number1, number2);
                        ps.AddCommand(f);
                        return valid;
                    case "Pen":
                        PenCommand g = (PenCommand)cf.GetCommand("pen");
                        g.Set(myCanvas, command[1]);
                        ps.AddCommand(g);
                        return valid;
                    case "rectangle":
                        RectangleCommand h = (RectangleCommand)cf.GetCommand("rectangle");
                        h.Set(myCanvas, number1, number2);
                        ps.AddCommand(h);
                        return valid;
                    case "triangle":
                        TriangleCommand i = (TriangleCommand)cf.GetCommand("triangle");
                        i.Set(myCanvas, number1);
                        ps.AddCommand(i);
                        return valid;
                    case "loop":
                        loopFlag = true;
                        ps.numberOfLoops = Int32.Parse(wholeCommand[2]);
                        return valid;
                    case "if":
                        IfCommand l = (IfCommand)cf.GetCommand("if");
                        ps.ifCommands.Add(commandInput);
                        ps.ifNumbers.Add(commandNumber);
                        ps.AddCommand(l);
                        return valid;
                    case "endif":
                        ps.ifNumbers.Add(commandNumber);
                        return valid;
                    case "method":
                        this.methodList.Add(wholeCommand[1]);
                        ps.methodStart = commandNumber;
                        methodFlag = true;
                        return valid;
                    case "while":
                        WhileCommand m = (WhileCommand)cf.GetCommand("while");
                        ps.AddCommand(m);
                        ps.whileCommands.Add(commandInput);
                        whileFlag = true;
                        return valid;
                    default:
                        throw new InvalidCommandException(command, commandNumber + 1);

                }
            }
            
        }

        /// <summary>
        /// Adds the commands in the for loop
        /// </summary>
        public void theLoop()
        {
            String[] theLoopCommands = ps.loopCommands.ToArray();
            int numOfLoops = ps.numberOfLoops;
            bool isValid = false;
            for (int i = 0; i < numOfLoops; i ++)
            {
                int commandNumber = 0;
                foreach(String loopComm in theLoopCommands)
                {
                    isValid = this.ValidCommand(loopComm, commandNumber);
                    commandNumber++;
                }
            }
        }

        /// <summary>
        /// When calling the method gets the method and adds them commands
        /// </summary>
        public void CallMethod()
        {
            String[] theMethodCommands = ps.methodCommands.ToArray();
            int methodLine = 0;
            foreach(String command in theMethodCommands)
            {
                this.ValidCommand(command, methodLine);

            }
        }



        /// <summary>
        /// Gets and sets the strings value of ReturnErrors
        /// </summary>
        public string ReturnErrors { get; set; }

    }

}

