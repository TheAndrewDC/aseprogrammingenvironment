﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// Invalid parameter Exceptions used to be thrown when invalid parameter is inserted
    /// </summary>
    [Serializable]
    public class InvalidParameterException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public InvalidParameterException()
        {

        }
        /// <summary>
        /// Throws the invalid parameter with custom message
        /// </summary>
        /// <param name="parameter">The invalid parameter</param>
        /// <param name="lineNumber">line which the parameter was on</param>
        public InvalidParameterException(String parameter, int lineNumber) : base(String.Format("\nInvalid Parameter: {0}. On Line {1}.", parameter, lineNumber.ToString()))
        {

        }
    }
}
