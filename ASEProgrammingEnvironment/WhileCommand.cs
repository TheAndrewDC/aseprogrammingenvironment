﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// While Command
    /// </summary>
    public class WhileCommand : Command
    {
        public int loopTotal;
        public int loopStartingLine;
        public Canvas _mycanvas;

        /// <summary>
        /// Set and get the starting line of the loop
        /// </summary>
        public int LoopStartingLine
        {
            get { return loopStartingLine; }
            set { loopStartingLine = value; }
        }
        /// <summary>
        /// Total number of loops
        /// </summary>
        public int LoopTotal
        {
            get { return loopTotal; }
            set { loopTotal = value; }
        }
        /// <summary>
        /// Set the canvas
        /// </summary>
        /// <param name="myCanvas">The Canvas</param>
        public void Set(Canvas myCanvas)
        {
            this._mycanvas = myCanvas;
        }

        /// <summary>
        /// Excecutes
        /// </summary>
        /// <returns>the canvas</returns>
        public override Canvas Execute()
        {
            return null;
        }
    }
}
