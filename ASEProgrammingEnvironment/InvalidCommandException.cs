﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// Invalid Command Exceptions used to be thrown when invalid command is made
    /// </summary>
    [Serializable]
    public class InvalidCommandException : Exception
    {
        /// <summary>
        /// Invalid command constructor
        /// </summary>
        public InvalidCommandException()
        {

        }
        /// <summary>
        /// Throws the invalid command with custom message
        /// </summary>
        /// <param name="command">the invalid command</param>
        /// <param name="lineNumber">line number the command was on</param>
        public InvalidCommandException(String command, int lineNumber) : base(String.Format("\nInvalid Command: {0}. On Line: {1}.", command, lineNumber.ToString()))
        {

        }
    }
}
