﻿namespace ASEProgrammingEnvironment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OutputDisplay = new System.Windows.Forms.PictureBox();
            this.LongCommandBox = new System.Windows.Forms.RichTextBox();
            this.ShortCommandBox = new System.Windows.Forms.TextBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.LoadButton = new System.Windows.Forms.Button();
            this.errorLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.OutputDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // OutputDisplay
            // 
            this.OutputDisplay.BackColor = System.Drawing.Color.Silver;
            this.OutputDisplay.Location = new System.Drawing.Point(12, 12);
            this.OutputDisplay.Name = "OutputDisplay";
            this.OutputDisplay.Size = new System.Drawing.Size(632, 530);
            this.OutputDisplay.TabIndex = 0;
            this.OutputDisplay.TabStop = false;
            this.OutputDisplay.Paint += new System.Windows.Forms.PaintEventHandler(this.OutputDisplay_Paint);
            // 
            // LongCommandBox
            // 
            this.LongCommandBox.Location = new System.Drawing.Point(671, 12);
            this.LongCommandBox.Name = "LongCommandBox";
            this.LongCommandBox.Size = new System.Drawing.Size(386, 401);
            this.LongCommandBox.TabIndex = 1;
            this.LongCommandBox.Text = "";
            // 
            // ShortCommandBox
            // 
            this.ShortCommandBox.Location = new System.Drawing.Point(671, 418);
            this.ShortCommandBox.Name = "ShortCommandBox";
            this.ShortCommandBox.Size = new System.Drawing.Size(386, 20);
            this.ShortCommandBox.TabIndex = 0;
            this.ShortCommandBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ShortCommandBox_KeyDown);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(671, 447);
            this.SaveButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(65, 29);
            this.SaveButton.TabIndex = 2;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // LoadButton
            // 
            this.LoadButton.Location = new System.Drawing.Point(740, 447);
            this.LoadButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(65, 29);
            this.LoadButton.TabIndex = 3;
            this.LoadButton.Text = "Load";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.Location = new System.Drawing.Point(680, 492);
            this.errorLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(0, 13);
            this.errorLabel.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1091, 632);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.LoadButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.ShortCommandBox);
            this.Controls.Add(this.LongCommandBox);
            this.Controls.Add(this.OutputDisplay);
            this.Name = "Form1";
            this.Text = "ASEProgammingEnvironment c3549195";
            ((System.ComponentModel.ISupportInitialize)(this.OutputDisplay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox OutputDisplay;
        private System.Windows.Forms.RichTextBox LongCommandBox;
        private System.Windows.Forms.TextBox ShortCommandBox;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.Label errorLabel;
    }
}

