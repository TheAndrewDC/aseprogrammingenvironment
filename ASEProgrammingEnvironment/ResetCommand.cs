﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// Reset command used for resetting position on the canvas
    /// </summary>
    class ResetCommand : Command
    {
        private Canvas _myCanvas;

        /// <summary>
        /// Reset Command constructor
        /// </summary>
        public ResetCommand()
        {
        }

        /// <summary>
        /// Reset Command constructor that sets the canvas
        /// </summary>
        /// <param name="myCanvas">Transfers Canvas to class</param>
        public ResetCommand(Canvas myCanvas) : base(myCanvas)
        {
            _myCanvas = myCanvas;
        }

        /// <summary>
        /// Set the canvas and the parameter list
        /// </summary>
        /// <param name="myCanvas">Sets the canvas</param>
        public void Set(Canvas myCanvas)
        {
            this._myCanvas = myCanvas;
        }

        /// <summary>
        /// Execute the drawing of the command
        /// </summary>
        /// <returns>Returns the canvas</returns>
        public override Canvas Execute()
        {
            _myCanvas.MoveTo(0, 0);
            return _myCanvas;
        }
    }
}
