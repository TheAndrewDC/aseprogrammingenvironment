﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// The main Canvas class.
    /// Contains all methods for performing drawing functions.
    /// </summary>
    public class Canvas
    {

        Graphics g;
        Pen Pen;
        Brush Filler;
        public String fillOrNot = "NoFill";
        public Canvas()
        {
        }
        public Canvas(Graphics g)
        {
            this.g = g;
            XLoc = 0;
            YLoc = 0;
            Pen = new Pen(Color.Black, 2);
            Filler = new SolidBrush(Color.Black);
        }
        /// <summary>
        /// Used for drawing a line between starting position and co-ordiantes provided by user.
        /// </summary>
        public void DrawLine(int xLoc2, int yLoc2)
        {
            g.DrawLine(Pen, XLoc, YLoc, xLoc2, yLoc2);
            this.MoveTo(xLoc2, yLoc2);
        }
        /// <summary>
        /// Used for positioning the pen on the canvas.
        /// </summary>
        public void MoveTo(int xLoc2, int yLoc2)
        {
            XLoc = xLoc2;
            YLoc = yLoc2;
        }
        /// <summary>
        /// Used for drawing a square or rectangle that can be filled or not.
        /// </summary>
        public void DrawSquare(int width, int height)
        {
            if (fillOrNot.Equals("on"))
            {
                g.FillRectangle(Filler, XLoc, YLoc, width, height);
            }
            else
            {
                g.DrawRectangle(Pen, XLoc, YLoc, width, height);
            }
        }
        /// <summary>
        /// Wipes the drawing
        /// </summary>
        public void ClearImage()
        {
            g.Clear(Color.Transparent);
        }
        /// <summary>
        /// Can draw a filled or unfilled circle based on radius given.
        /// </summary>
        public void DrawCircle(int radius)
        {
            radius = radius * 2;
            if (fillOrNot.Equals("on"))
            {
                g.FillEllipse(Filler, XLoc - radius / 2, YLoc - radius / 2, radius, radius);
            }
            else
            {
                g.DrawEllipse(Pen, XLoc - radius / 2, YLoc - radius / 2, radius, radius);
            }

        }

        /// <summary>
        /// Used to draw a filled or unfilled triangle from a given size
        /// </summary>
        public void DrawTriangle(int size)
        {
            PointF point1 = new PointF(XLoc, YLoc);
            PointF point2 = new PointF(XLoc + size, YLoc);
            PointF point3 = new PointF(XLoc, YLoc + size);
            PointF[] trianglePoints = { point1, point2, point3 };

            if (fillOrNot.Equals("on"))
            {
                g.FillPolygon(Filler, trianglePoints);
            }
            else
            {
                g.DrawPolygon(Pen, trianglePoints);
            }

        }
        /// <summary>
        /// Sets the pen and brush colour
        /// </summary>
        /// <param name="colour">String used for storing what colour the pen and brush will be</param>
        /// <returns>Returns the colour value</returns>
        public string SetPenColour(string colour)
        {
            Pen = new Pen(Color.FromName(colour));
            Filler = new SolidBrush(Color.FromName(colour));
            return colour;
        }


        /// <summary>
        /// Used for checking wether a shape should be filled or not
        /// </summary>
        /// <param name="fillOrNotStatement">String that stores the state if the shape should be filled or not</param>
        public void FillOrNot(string fillOrNotStatement)
        {
            fillOrNot = fillOrNotStatement;
        }


        /// <summary>
        /// Gets and sets the XLoc integer
        /// </summary>
        public int XLoc { get; set; }
        /// <summary>
        /// Gets and sets the YLoc integer
        /// </summary>
        public int YLoc { get; set; }
    }
}
