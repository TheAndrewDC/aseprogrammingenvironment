﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// Circle command used for setting circle parameter and then executing
    /// </summary>
    class CircleCommand : Command
    {
        private Canvas _myCanvas;


        /// <summary>
        /// Circle Command constructor
        /// </summary>
        public CircleCommand()
        {
        }

        /// <summary>
        /// Circle Command constructor that sets the canvas
        /// </summary>
        /// <param name="myCanvas">Transfers Canvas to class</param>
        public CircleCommand(Canvas myCanvas) : base(myCanvas)
        {
            _myCanvas = myCanvas;
        }

        /// <summary>
        /// Set the canvas and the parameter list
        /// </summary>
        /// <param name="myCanvas">Sets the canvas</param>
        /// <param name="list">Sets the parameter</param>
        public override void Set(Canvas myCanvas, params int[] list)
        {
            this._myCanvas = myCanvas;
            base.ParamsInt = list;
        }

        /// <summary>
        /// Execute the drawing of the command
        /// </summary>
        /// <returns>Returns the canvas</returns>
        public override Canvas Execute()
        {
            _myCanvas.DrawCircle(ParamsInt[0]);
            return _myCanvas;
        }

    }
}
