﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// DrawTo command used for setting drawto parameters and then executing
    /// </summary>
    class DrawToCommand : Command
    {
        private Canvas _myCanvas;

        /// <summary>
        /// Draw to command constructor
        /// </summary>
        public DrawToCommand()
        {
        }

        /// <summary>
        /// Draw To Command constructor that sets the canvas
        /// </summary>
        /// <param name="myCanvas">Transfers Canvas to class</param>
        public DrawToCommand(Canvas myCanvas) : base(myCanvas)
        {
            _myCanvas = myCanvas;
        }

        /// <summary>
        /// Set the canvas and the parameter list
        /// </summary>
        /// <param name="myCanvas">Sets the canvas</param>
        /// <param name="list">Sets the parameter</param>
        public override void Set(Canvas myCanvas, params int[] list)
        {
            this._myCanvas = myCanvas;
            base.ParamsInt = list;
        }

        /// <summary>
        /// Execute the drawing of the command
        /// </summary>
        /// <returns>Returns the canvas</returns>
        public override Canvas Execute()
        {
            _myCanvas.DrawLine(ParamsInt[0], ParamsInt[1]);
            return _myCanvas;
        }

    }
}
