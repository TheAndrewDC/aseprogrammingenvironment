﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// Pen command used for setting pen parameter and then executing
    /// </summary>
    class PenCommand : Command
    {
        private string _text;
        private Canvas _myCanvas;

        /// <summary>
        /// Pen Command constructor
        /// </summary>
        public PenCommand()
        {
        }

        /// <summary>
        /// Pen Command constructor that sets the canvas
        /// </summary>
        /// <param name="myCanvas">Transfers Canvas to class</param>
        public PenCommand(Canvas myCanvas) : base(myCanvas)
        {
            _myCanvas = myCanvas;
        }

        /// <summary>
        /// Set the canvas and the parameter list
        /// </summary>
        /// <param name="myCanvas">Sets the canvas</param>
        /// <param name="colour">Sets the parameter</param>
        public void Set(Canvas myCanvas, string colour)
        {
            this._myCanvas = myCanvas;
            this._text = colour;
        }

        /// <summary>
        /// Execute the drawing of the command
        /// </summary>
        /// <returns>Returns the canvas</returns>
        public override Canvas Execute()
        {
            _myCanvas.SetPenColour(_text);
            return _myCanvas;
        }

    }
}
