﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// Rectangle command used for setting rectangle parameters and then executing
    /// </summary>
    class RectangleCommand : Command
    {
        private Canvas _myCanvas;

        /// <summary>
        /// Rectangle Command constructor
        /// </summary>
        public RectangleCommand()
        {
        }

        /// <summary>
        /// Rectangle Command constructor that sets the canvas
        /// </summary>
        /// <param name="myCanvas">Transfers Canvas to class</param>
        public RectangleCommand(Canvas myCanvas) : base(myCanvas)
        {
            _myCanvas = myCanvas;
        }

        /// <summary>
        /// Set the canvas and the parameter list
        /// </summary>
        /// <param name="myCanvas">Sets the canvas</param>
        /// <param name="list">Sets the parameter</param>
        public override void Set(Canvas myCanvas, params int[] list)
        {
            this._myCanvas = myCanvas;
            base.ParamsInt = list;
        }

        /// <summary>
        /// Execute the drawing of the command
        /// </summary>
        /// <returns>Returns the canvas</returns>
        public override Canvas Execute()
        {
            _myCanvas.DrawSquare(ParamsInt[0], ParamsInt[1]);
            return _myCanvas;
        }

    }
}
