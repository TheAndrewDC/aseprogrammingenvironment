﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// Method Command
    /// </summary>
    class MethodCommand : Command
    {
        public List<String> ifCommands = new List<String>();
        public String[] stringHere;
        /// <summary>
        /// Constructor
        /// </summary>
        public MethodCommand()
        {

        }
        /// <summary>
        /// Get the method commands
        /// </summary>
        /// <returns>return the commands</returns>
        public String[] getMethodCommands()
        {
            stringHere = ifCommands.ToArray();
            return stringHere;
        }

        /// <summary>
        /// Add method command
        /// </summary>
        /// <param name="newCommand">The command to be added</param>
        public void AddMethodCommand(string newCommand)
        {
            ifCommands.Add(newCommand);
        }
        /// <summary>
        /// Excecutes
        /// </summary>
        /// <returns>returns canvas</returns>
        public override Canvas Execute()
        {
            return myCanvas;
        }


    }
}
