﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingEnvironment
{
    /// <summary>
    /// Fill command used for setting fill parameter and then executing
    /// </summary>
    class FillCommand : Command
    {
        private string _text;
        private Canvas _myCanvas;

        /// <summary>
        /// Fill Command constructor
        /// </summary>
        public FillCommand()
        {
        }

        /// <summary>
        /// Fill Command constructor that sets the canvas
        /// </summary>
        /// <param name="myCanvas">Transfers Canvas to class</param>
        public FillCommand(Canvas myCanvas) : base(myCanvas)
        {
            _myCanvas = myCanvas;
        }

        /// <summary>
        /// Set the canvas and the parameter list
        /// </summary>
        /// <param name="myCanvas">Sets the canvas</param>
        /// <param name="list">Sets the parameter</param>
        public void Set(Canvas myCanvas, string fillOrNot)
        {
            this._myCanvas = myCanvas;
            this._text = fillOrNot;
        }

        /// <summary>
        /// Execute the drawing of the command
        /// </summary>
        /// <returns>Returns the canvas</returns>
        public override Canvas Execute()
        {
            _myCanvas.FillOrNot(_text);
            return _myCanvas;
        }
    }
}
