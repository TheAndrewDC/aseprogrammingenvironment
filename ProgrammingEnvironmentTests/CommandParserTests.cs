using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASEProgrammingEnvironment;
using System;

namespace ProgrammingEnvironmentTests
{
    [TestClass]
    public class CommandParserTests
    {
        //Part 1 Tests
        /**
        [TestMethod]
        public void TestMoveToReturnsInvalid()
        {
            //Arrange
            CommandParser parser = new CommandParser();
            string command = "moveto 100,asd";
            string errors = "";
            //Act
            try
            {
                parser.ValidCommand(command);
            }
            catch (ApplicationException e)
            {
                errors = e.Message;
            }
            //Assert
            Assert.AreEqual("\nInvalid Parameter: 100,asd", errors);
        }


        [TestMethod]
        public void TestCircleReturnsInvalid()
        {
            //Arrange
            CommandParser parser = new CommandParser();
            string command = "circle";
            string errors = "";
            //Act
            try
            {
                parser.ValidCommand(command);
            }
            catch (ApplicationException e)
            {
                errors = e.Message;
            }
            //Assert
            Assert.AreEqual("\nInvalid Command: circle", errors);
        }

        [TestMethod]

        public void TestPenReturnsInvalid()
        {
            //Arrange
            CommandParser parser = new CommandParser();
            string command = "pen dsw";
            string errors = "";
            //Act
            try
            {
                parser.ValidCommand(command);
            }
            catch (ApplicationException e)
            {
                errors = e.Message;
            }
            //Assert
            Assert.AreEqual("\nInvalid Parameter: dsw", errors);
        }

        [TestMethod]

        public void TestLoopCommandFail()
        {
            //Arrange
            CommandParser parser = new CommandParser();
            string[] commands = new string[] { "loop 3", "moveto 20,ss", "drawto 0,0", "end"};
            string errors = "";

            //Act
            try
            {
                parser.ParserLoop(commands, 3);
            }
            catch (ApplicationException e)
            {
                errors =  e.Message;
            }
            //Assert
            Assert.AreEqual("\nInvalid Parameter: 20,ss\nInvalid Parameter: 20,ss\nInvalid Parameter: 20,ss", errors);
        }



    }

    [TestClass]
    public class CanvasTests
    {
        [TestMethod]

        public void TestMoveToSuccessfullyChangesXandYValue()
        {
            //Arrange
            Canvas canvas = new Canvas();
            int xLoc = 80;
            int yLoc = 75;
            //Act
            canvas.MoveTo(xLoc, yLoc);
            //Assert
            Assert.AreEqual(canvas.XLoc, 80);
            Assert.AreEqual(canvas.YLoc, 75);
        }

        [TestMethod]

        public void TestSetFillOn()
        {
            //Arrange
            Canvas canvas = new Canvas();
            //Act
            canvas.FillOrNot ("Fill");
            //Assert
            Assert.AreEqual(canvas.FillOrNot(), "Fill");
        }
        **/

        /// <summary>
        /// Test that puts in an invalid command and a InvalidCommandException is thrown
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidCommandException))]
        public void PassInvalidCommand_GetInvalidCommandExceptionBack()
        {
            //Arrange
            CommandParser parser = new CommandParser();
            String theInvalidCommand = "circl 100";
            int commandnumber = 0;

            //Act
            parser.ValidCommand(theInvalidCommand, commandnumber);
        }


        /// <summary>
        /// Test that puts in an invalid parameter and a InvalidParameterException is thrown
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidParameterException))]
        public void PassInvalidParameter_GetInvalidParameterExceptionBack()
        {
            //Arrange
            CommandParser parser = new CommandParser();
            String theInvalidParameter = "circle q";
            int commandNumber = 0;

            //Act
            parser.ValidCommand(theInvalidParameter, commandNumber);
        }

        /// <summary>
        /// Test that puts creates variable and gets the value
        /// </summary>
        [TestMethod]
        public void PassVariableWithNumber_GetVariableValue()
        {
            //Arrange
            Variables variable = new Variables("radius", 100);
            ProgramStorage ps = new ProgramStorage();
            int varValue;

            //Act
            ps.AddVariable(variable);
            varValue = ps.GetVariableValue("radius");

            //Assert
            Assert.AreEqual(varValue, 100);          
        }
        /// <summary>
        /// Test that puts creates variable then changes it to check the value changes 
        /// </summary>
        [TestMethod]
        public void ChangeVariableValue()
        {
            //Arrange
            ProgramStorage ps = new ProgramStorage();
            String varName = "radius";
            int initialVarValue = 100;
            int newVarValue = 200;
            int varValue;

            //Act
            Variables variable = new Variables(varName, initialVarValue);
            ps.AddVariable(variable);
            ps.SetVariableValue(varName, newVarValue);
            varValue = ps.GetVariableValue(varName);

            //Assert
            Assert.AreEqual(varValue, 200);

        }

        /// <summary>
        /// Tests that all of the commands return valid;
        /// </summary>
        [TestMethod]
        public void AllCommandsReportedValid()
        {
            //Arrange
            String[] commands = { "circle 100", "triangle 50", "moveto 100,100", "radius = 50", "circle radius" };
            CommandParser parser = new CommandParser();
            int commandNumber = 0;
            bool isValid = false;
            bool isNotValid = false;

            //Act
            foreach(String command in commands)
            {
                try
                {
                    isValid = parser.ValidCommand(command, commandNumber);
                }
                catch (InvalidCommandException)
                {
                    isNotValid = true;
                }
                catch (InvalidParameterException)
                {
                    isNotValid = true;
                }
                commandNumber++;
            }


            //Assert
            Assert.AreEqual(isNotValid, false);

        }


    }
}

